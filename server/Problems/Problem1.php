<?php

namespace Server\Problems;

/**
 * Finish implementation of Class Problem1 by having the method it must implement return the
 * solution to the following problem:
 *
 * Given an array of integers sorted in ascending order, find the starting and ending position of a given target value.
 *
 * Your algorithm's runtime complexity must be in the order of O(log n).
 *
 * If the target is not found in the array, return [-1, -1].
 *
 * For example, given
 * $params[0] = [5, 7, 7, 8, 8, 10]
 * and target value
 * $params[1] = 8
 *
 * return [3, 4]
 *
 */
abstract class Problem1 implements Problem
{
    public function run(...$params) {
        $result = [array_search($params[1], $params[0]), array_search($params[1], array_reverse($params[0], true))];
        return array_map(function(){ return $i ? $i : -1; }, $result);
    }
}