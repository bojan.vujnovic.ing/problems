# Description

To start a probject install docker from `https://www.docker.com/docker-ubuntu` and docker compose from `https://docs.docker.com/compose/install/`;
Start project by entering `docker compose up` in your terminal in this folder. This should install all dependencies and it should start backend and frontend servers.
After project finishes installation open `http://localhost:4200` in browser.

## Problem files

All Problem files are located in `/server/Problems`;