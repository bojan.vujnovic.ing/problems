import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { LogModel } from './log.model';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/interval';
import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/timeInterval';
import 'rxjs/add/operator/mergeMap';

@Injectable()
export class LogsService {

  constructor(
    private http: HttpClient
  ) { }

  getData(): Observable<LogModel[]> {
    return Observable.interval(1000*5)
      .startWith(0)
      .timeInterval()
      .mergeMap(() => this.http.get('http://localhost:5050/logs'))
      .map((data: LogModel[]) => data);
  }

}
