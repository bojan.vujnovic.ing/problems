<?php

namespace Server\Problems;

/**
 * Finish implementation of Class Problem3 by having the method it must implement return the
 * solution to the following problem:
 *
 * You have an array for which the ith element is the price of a given stock on day i.
 *
 * If you were only permitted to complete at most one transaction (ie, buy one and sell one share of the stock),
 * design an algorithm to find the maximum profit.
 *
 * For example:
 *
 * $params[0] = [7, 1, 5, 3, 6, 4]
 * return 5
 *
 * $params[0] = [7, 6, 4, 3, 1]
 * return 0
 *
 */
abstract class Problem3 implements Problem
{
    public function run(...$params) {
        $minIndex = array_search(min($params[0]), $params[0]);
        return max(array_slice($params[0], $minIndex)) - $params[0][$minIndex];
    }
}