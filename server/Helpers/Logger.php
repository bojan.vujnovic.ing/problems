<?php

namespace Server\Helpers;

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class LogSys {

    public static $instance;

    public static $logger;

    public static function log($status, $log){

        if(self::$instance === null) {
            self::$instance = new LogSys();
        }

        if(self::$instance::$logger === null) {
            $logger = new Logger('issue_logger');
            $logPath = realpath(__DIR__.'/../Logs/') . '/cron.log';
            $logger->pushHandler(new StreamHandler($logPath, Logger::DEBUG));
            self::$instance::$logger = $logger;
        }

        $newLog = \ORM::for_table('logs')->create();
        $newLog->time = date_create()->format('Y-m-d H:i:s');
        $newLog->status = $status;
        $newLog->description = $log['description'];
        $newLog->save();
        self::$instance::$logger->{strtolower($status)}($status, $log);
    }

}