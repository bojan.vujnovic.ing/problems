<?php

namespace Server\Problems;

use OzdemirBurak\JsonCsv\File\Json;
use NestedJsonFlattener\Flattener\Flattener;
use Server\Helpers\LogSys;
use \Slim\Http\Stream;

/**
 * TourRadar's Head of Marketing (who's not a technical expert) has asked you to design a system that:
 * - pulls data from a list of JSON sources provided to you
 * - converts each JSON file to CSV and saves it to disk. The CSV should have a header row and the following data (provided below)
 * - regularly checks for updates so that if the JSON source has changed, the CSV file on the disk should be automatically updated
 * - has a relatively easy way to add new JSON sources, ideally without even touching your code
 *
 * Specifically, they ask you to build the initial system around these three URLs:
 * https://data.cityofnewyork.us/api/views/kku6-nxdu/rows.json?accessType=DOWNLOAD
 * https://chronicdata.cdc.gov/api/views/5svk-8bnq/rows.json?accessType=DOWNLOAD
 * https://data.cityofchicago.org/api/views/xzkq-xp2w/rows.json?accessType=DOWNLOAD
 *
 * The output of the initial system should be three CSV files on a disk.
 *
 *
 * Extra (optional):
 * - have the system protect against human errors
 * - have logging so you can debug and let the marketing team know if something breaks
 * - have a notification system if the update fails or the link is broken/inaccessible
 * - have support of JSON files of different structures
 * - have other data formats supported (e.g. XML, CSV, TSV)
 */
class Problem0 implements Problem
{

    private $destinations = __DIR__.'/urls.json';

    public function run(...$params) {
    }

    public function runApi() {
        $app = new \Slim\App([
            'settings' => [
                'displayErrorDetails' => true
            ]
        ]);

        $app->options('/{routes:.+}', function ($request, $response, $args) {
            return $response;
        });
        
        $app->add(function ($req, $res, $next) {
            $response = $next($req, $res);
            return $response
                    ->withHeader('Access-Control-Allow-Origin', 'http://localhost:4200')
                    ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
                    ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, PATCH, OPTIONS');
        });

        $context = $this;

        $app->get('/', function ($request, $response, $args) use($context){
            $response->getBody()->write(var_dump($context->run(1,2,3)));
        });

        $app->get('/data', function ($request, $response, $args){
            try{
                $destinations = \ORM::for_table('destinations')->find_array();
                return $response->withJson($destinations, 200);
            } catch (Exception $exception) {
                LogSys::log('ERROR', [ 'location' => 'Problem0', 'action' => 'get', 'description' => $exception->getMessage()]);
                return $response->withJson(array( 'error' => $exception->getMessage() ), 500);
            }
        });

        $app->get('/resource/{alias}', function($request, $response, $args){
            $file = realpath(__DIR__ . '/../Csv/') . '/' . $request->getAttribute('alias') . '.csv';
            if(file_exists($file)) {
                $fh = fopen($file, 'rb');
                $stream = new Stream($fh);

                return $response->withHeader('Content-Type', 'application/force-download')
                                ->withHeader('Content-Type', 'application/octet-stream')
                                ->withHeader('Content-Type', 'application/download')
                                ->withHeader('Content-Description', 'File Transfer')
                                ->withHeader('Content-Transfer-Encoding', 'binary')
                                ->withHeader('Content-Disposition', 'attachment; filename="' . basename($file) . '"')
                                ->withHeader('Expires', '0')
                                ->withHeader('Cache-Control', 'must-revalidate, post-check=0, pre-check=0')
                                ->withHeader('Pragma', 'public')
                                ->withBody($stream);
            } else {
                return $response->getBody()->write('Resource not updated yet or it doesn\'t exist');
            }
        });

        $app->post('/add', function ($request, $response, $args) {
            try{
                $destination = \ORM::for_table('destinations')->create();
                $destination->set($request->getParams());
                $destination->alias = preg_replace('/[^A-Za-z0-9-]+/i', '-', $destination->name);
                $destination->save();
                LogSys::log('INFO', [ 'location' => 'Problem0', 'action' => 'add', 'description' => 'Added ' . $destination->name]);
                return $response->withJson($destination->as_array(), 200);
            } catch (Exception $exception) {
                LogSys::log('ERROR', [ 'location' => 'Problem0', 'action' => 'add', 'description' => $exception->getMessage()]);
                return $response->withJson(array( 'error' => $exception->getMessage() ), 500);
            }
        });

        $app->post('/delete', function ($request, $response, $args){
            try{
                $destination = \ORM::for_table('destinations')->find_one($request->getParam('id'));
                $destination->delete();
                LogSys::log('INFO', [ 'location' => 'Problem0', 'action' => 'delete', 'description' => 'Deleted ' . $destination->name]);
                return $response->withJson($destination->as_array(), 200);
            } catch (Exception $exception) {
                LogSys::log('ERROR', [ 'location' => 'Problem0', 'action' => 'delete', 'description' => $exception->getMessage()]);
                return $response->withJson(array( 'error' => $exception->getMessage() ), 500);
            }
        });

        $app->get('/logs', function ($request, $response, $args){
            try{
                $yesterday = date('Y-m-d H:i:s', strtotime( '-1 days' ));
                $logs = \ORM::for_table('logs')->where_gt('time', $yesterday)->order_by_desc('time')->limit(10)->find_array();
                return $response->withJson($logs, 200);
            } catch (Exception $exception) {
                LogSys::log('ERROR', [ 'location' => 'Problem0', 'action' => 'logs', 'description' => $exception->getMessage()]);
                return $response->withJson(array( 'error' => $exception->getMessage() ), 500);
            }
        });

        $app->map(['GET', 'POST', 'PUT', 'DELETE', 'PATCH'], '/{routes:.+}', function($req, $res) {
            return $res->withJson([ 'status' => 'not found' ], 404);
        });

        $app->run();
    }

    public function updateData() {
        $destinations = \ORM::for_table('destinations')->find_many();

        foreach($destinations as $destination) {
            try {
                $jsonData = file_get_contents($destination->url);
                $storagePath = realpath(__DIR__.'/../Csv/');
                $destinationPath = $storagePath . '/' . $destination->alias;
                $flattener = new Flattener();
                $flattener->setJsonData($jsonData);
                $flattener->writeCsv($destinationPath);
                LogSys::log('INFO', [ 'location' => 'Problem0', 'action' => 'updateData', 'description' => 'Updated ' . $destination->alias.'.csv']);
            } catch (Exception $exception) {
                LogSys::log('ERROR', [ 'location' => 'Problem0', 'action' => 'updateData', 'desctiption' => $exception->getMessage()]);
                return $response->getBody()->write($exception->getMessage());
            }
        }
    }
    
}