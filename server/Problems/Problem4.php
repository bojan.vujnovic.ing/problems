<?php

namespace Server\Problems;

/**
 * Finish implementation of Class Problem4 by having the method it must implement return the
 * solution to the following problem:
 *
 * Find all valid IP address combinations within a given string containing only digits.
 *
 * For example:
 *
 * $params[0] = '25525511135'
 * return ['255.255.11.135', '255.255.111.35']
 *
 */
abstract class Problem4 implements Problem
{
    public function run(...$params) {
        return $this->generateIps($params[0]);
    }

    private function generateIps(string $nums) {
        $ips = [];
        $length = strlen($nums);
        for($first = 1; $first < $length - 2; $first++) {
            for($second = $first +1; $second < $length -1; $second++) {
                for($third = $second +1; $third < $length; $third++) {                    
                    $ip = implode('.', [
                        substr($nums, 0, $first - $length),
                        substr($nums, $first, $second - $length),
                        substr($nums, $second, $third - $length),
                        substr($nums, $third)
                    ]);
                    if($this->isValidIp($ip)) { $ips[] = $ip; }
                }
            }
        }

        return $ips;
    }

    private function isValidIpPart(string $part): bool {
        if ((int) $part < 0 || (int) $part > 255 || preg_match('/^0\d+/', $part)) { return false; }
        return true;
    }

    private function isValidIp(string $ip){
        if(!preg_match("/^(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})$/",$ip)) { return false; }
        foreach(explode(".", $ip) as $part) { if(!$this->isValidIpPart($part)) return false; }
        return true;
    }
}