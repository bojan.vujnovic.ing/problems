<?php

namespace Server;

class Autoloader {

    public static function require($dir) {
        foreach (glob("$dir/*") as $path) {
            if (preg_match('/\.php$/', $path)) {
                require_once $path;
            }
            elseif (is_dir($path)) {
                self::require($path);
            }
        }
    }

    public static function require_all(...$dir) {
        foreach($dir as $path) {
            self::require($path);
        }
    }
}