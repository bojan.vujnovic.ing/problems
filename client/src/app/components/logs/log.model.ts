export enum Status {
    ERROR = "error",
    SUCCESS = "info"
}

export class LogModel {
    id: string;
    time: string;
    description: string;
    status: Status;
}