<?php

require_once(realpath(__DIR__.'/../').'/config.php');

use Server\Problems\Problem0;
use Server\Helpers\LogSys;

class CronJob {

    public function __construct($job)
    {
        $this->{$job}();
    }

    private function updateData() {
        $problem = new Problem0;
        $problem->updateData();
    }

}