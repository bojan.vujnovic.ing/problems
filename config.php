<?php

require_once(__DIR__.'/vendor/autoload.php');
require_once(__DIR__.'/server/autoload.php');

Server\Autoloader::require_all(
    __DIR__.'/server/Helpers',
    __DIR__.'/server/Problems'
);

ORM::configure('mysql:host=database;dbname=problems');
ORM::configure('username', 'root');
ORM::configure('password', 'admin');