import { Component, OnInit } from '@angular/core';
import { ListService } from './list.service';
import { Observable } from 'rxjs/Observable';
import { DataModel } from './data.model';
import { DialogComponent } from '../dialog/dialog.component';
import { MatDialog } from '@angular/material';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  
  constructor(
    private dataService: ListService,
    public dialog: MatDialog
  ){ }

  data: BehaviorSubject<Array<DataModel>> = new BehaviorSubject(null);

  newItemDialog() {
    let dialogRef = this.dialog.open(DialogComponent, {
      width: '500px',
      data: {}
    });

    dialogRef.afterClosed().subscribe(result => this.addItem(result));
  }

  addItem(data: DataModel) {
    this.dataService.addResource(data)
      .subscribe(newData => this.data.next(this.data.value.concat([newData])));
  }

  removeItem(data: DataModel) {
    this.dataService.removeResource(data)
      .subscribe(oldData => this.data.next(this.data.value.filter(({id}) => id !== oldData.id)));
  }

  ngOnInit() {
    this.dataService.getData()
      .subscribe(data => this.data.next(data));
  }

}
