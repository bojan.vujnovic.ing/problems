#!/bin/bash
#Update package lists
if ! [ -x "$(command -v composer)" ]; then
    apt-get update
    apt-get install -y curl git
    curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
    composer install

    docker-php-ext-install pdo pdo_mysql

    apt-get install -y cron
    crontab /var/www/html/crontab

    chmod 777 -R /var/www/html/server

    echo 'ServerName localhost' >> /etc/apache2/apache2.conf
    a2enmod rewrite
fi

service cron restart

echo "///////////////////////////////////////////////////////////////////";
echo "///////////////////////////////////////////////////////////////////";
echo "////////        ////////       //////   /////  ///           //////";
echo "///////  /////   //////  /////  ////    ////  ///  ////////////////";
echo "//////  ///////   ///  ///////  ///  /  ///  ///  /////////////////";
echo "/////  ////////   //  ////////  //  //  //  ///      //////////////";
echo "////  ////////   ///  ///////  //  ///  /  ///  ///////////////////";
echo "///  //////   //////  /////  ///  ////    ///  ////////////////////";
echo "//          /////////      ////  /////   ///           ////////////";
echo "///////////////////////////////////////////////////////////////////";

source /etc/apache2/envvars
exec apache2 -D FOREGROUND