-- MySQL dump 10.13  Distrib 5.7.9, for Linux (x86_64)
--
-- Host: localhost    Database: problems
-- ------------------------------------------------------
-- Server version	5.7.9

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `destinations`
--

DROP TABLE IF EXISTS `destinations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `destinations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `destinations`
--

LOCK TABLES `destinations` WRITE;
/*!40000 ALTER TABLE `destinations` DISABLE KEYS */;
INSERT INTO `destinations` VALUES (1,'New York','https://data.cityofnewyork.us/api/views/kku6-nxdu/rows.json?accessType=DOWNLOAD','new_york'),(2,'Cronic Data','https://chronicdata.cdc.gov/api/views/5svk-8bnq/rows.json?accessType=DOWNLOAD','cronic_data'),(17,'Chicago','https://data.cityofchicago.org/api/views/xzkq-xp2w/rows.json?accessType=DOWNLOAD','Chicago');
/*!40000 ALTER TABLE `destinations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `logs`
--

DROP TABLE IF EXISTS `logs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `logs` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `logs`
--

LOCK TABLES `logs` WRITE;
/*!40000 ALTER TABLE `logs` DISABLE KEYS */;
INSERT INTO `logs` VALUES (4,'2018-05-11 22:30:03','Updated new_york.csv','INFO'),(5,'2018-05-11 22:30:07','Updated cronic_data.csv','INFO'),(6,'2018-05-11 22:31:02','Updated new_york.csv','INFO'),(7,'2018-05-11 22:31:06','Updated cronic_data.csv','INFO'),(8,'2018-05-11 22:32:03','Updated new_york.csv','INFO'),(9,'2018-05-11 22:32:06','Updated cronic_data.csv','INFO'),(10,'2018-05-11 22:33:03','Updated new_york.csv','INFO'),(11,'2018-05-11 22:33:06','Updated cronic_data.csv','INFO'),(12,'2018-05-11 22:34:02','Updated new_york.csv','INFO'),(13,'2018-05-11 22:34:06','Updated cronic_data.csv','INFO'),(14,'2018-05-11 22:35:03','Updated new_york.csv','INFO'),(15,'2018-05-11 22:35:06','Updated cronic_data.csv','INFO'),(16,'2018-05-11 22:36:02','Updated new_york.csv','INFO'),(17,'2018-05-11 22:36:06','Updated cronic_data.csv','INFO'),(18,'2018-05-11 22:37:03','Updated new_york.csv','INFO'),(19,'2018-05-11 22:37:06','Updated cronic_data.csv','INFO'),(20,'2018-05-11 22:38:02','Updated new_york.csv','INFO'),(21,'2018-05-11 22:38:06','Updated cronic_data.csv','INFO'),(22,'2018-05-11 22:39:02','Updated new_york.csv','INFO'),(23,'2018-05-11 22:39:06','Updated cronic_data.csv','INFO'),(24,'2018-05-11 22:40:02','Updated new_york.csv','INFO'),(25,'2018-05-11 22:40:06','Updated cronic_data.csv','INFO'),(26,'2018-05-11 22:41:02','Updated new_york.csv','INFO'),(27,'2018-05-11 22:41:06','Updated cronic_data.csv','INFO'),(28,'2018-05-11 22:42:03','Updated new_york.csv','INFO'),(29,'2018-05-11 22:42:06','Updated cronic_data.csv','INFO'),(30,'2018-05-11 22:43:03','Updated new_york.csv','INFO'),(31,'2018-05-11 22:43:06','Updated cronic_data.csv','INFO'),(32,'2018-05-11 22:43:28','Added asdasd','INFO'),(33,'2018-05-11 22:43:32','Deleted asdasd','INFO'),(34,'2018-05-11 22:44:03','Updated new_york.csv','INFO'),(35,'2018-05-11 22:44:06','Updated cronic_data.csv','INFO'),(36,'2018-05-11 22:44:46','Added asdasd','INFO'),(37,'2018-05-11 22:44:48','Deleted asdasd','INFO'),(38,'2018-05-11 22:45:02','Updated new_york.csv','INFO'),(39,'2018-05-11 22:45:06','Updated cronic_data.csv','INFO'),(40,'2018-05-11 22:45:21','Added adsad','INFO'),(41,'2018-05-11 22:45:22','Deleted adsad','INFO'),(42,'2018-05-11 22:46:03','Updated new_york.csv','INFO'),(43,'2018-05-11 22:46:06','Updated cronic_data.csv','INFO'),(44,'2018-05-11 22:47:02','Updated new_york.csv','INFO'),(45,'2018-05-11 22:47:06','Updated cronic_data.csv','INFO'),(46,'2018-05-11 22:48:02','Updated new_york.csv','INFO'),(47,'2018-05-11 22:48:06','Updated cronic_data.csv','INFO'),(48,'2018-05-11 22:49:03','Updated new_york.csv','INFO'),(49,'2018-05-11 22:49:06','Updated cronic_data.csv','INFO'),(50,'2018-05-11 23:00:40','Added sadad','INFO'),(51,'2018-05-11 23:00:42','Deleted sadad','INFO'),(52,'2018-05-11 23:10:42','Added asd','INFO'),(53,'2018-05-11 23:10:45','Deleted asd','INFO'),(54,'2018-05-11 23:18:38','Added asd','INFO'),(55,'2018-05-11 23:18:42','Deleted asd','INFO'),(56,'2018-05-12 00:09:03','Added asdasd','INFO'),(57,'2018-05-12 00:09:06','Deleted asdasd','INFO'),(58,'2018-05-12 00:09:23','Added Bojan','INFO'),(59,'2018-05-12 00:09:26','Deleted Bojan','INFO'),(60,'2018-05-12 00:46:11','Added Chicago','INFO');
/*!40000 ALTER TABLE `logs` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-12 12:31:57
