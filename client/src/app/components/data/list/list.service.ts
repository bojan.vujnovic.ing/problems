import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { DataModel } from './data.model';
import 'rxjs/add/operator/map';

@Injectable()
export class ListService {

  constructor(
    private http: HttpClient
  ) {
  }

  getData(): Observable<Array<DataModel>> {
    return this.http.get('http://localhost:5050/data')
      .map((data: Array<DataModel>) => data);
  }

  addResource(data: DataModel): Observable<DataModel> {
    return this.http.post('http://localhost:5050/add', data)
      .map((data: DataModel) => data);
  }

  removeResource(data: DataModel): Observable<DataModel> {
    return this.http.post('http://localhost:5050/delete', data)
      .map((data: DataModel) => data);
  }

}
