import { Component } from '@angular/core';
import { LogsService } from './logs.service';
import { LogModel } from './log.model';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-logs',
  templateUrl: './logs.component.html',
  styleUrls: ['./logs.component.scss']
})
export class LogsComponent {

  constructor(
    private dataService: LogsService
  ) { }
  
  logs: Observable<LogModel[]> = this.dataService.getData();

}
