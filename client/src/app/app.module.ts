import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { AppRoutingModule } from './app-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { ListService } from './components/data/list/list.service';
import { MatListModule, MatIconModule, MatButtonModule, MatDialogModule, MatFormFieldModule, MatInputModule, MatCardModule, MatToolbarModule } from '@angular/material';
import { DialogComponent } from './components/data/dialog/dialog.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ListComponent } from './components/data/list/list.component';
import { MainComponent } from './layouts/main/main.component';
import { HomeComponent } from './pages/home/home.component';
import { LogsComponent } from './components/logs/logs.component';
import { LogsService } from './components/logs/logs.service';

@NgModule({
  declarations: [
    AppComponent,
    DialogComponent,
    ListComponent,
    MainComponent,
    HomeComponent,
    LogsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    CommonModule,
    BrowserAnimationsModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    FlexLayoutModule,
    MatCardModule,
    MatToolbarModule
  ],
  providers: [
    ListService,
    LogsService
  ],
  entryComponents: [DialogComponent],
  bootstrap: [AppComponent],
})
export class AppModule { }
