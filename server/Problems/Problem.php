<?php

namespace Server\Problems;

interface Problem
{
    public function run(...$params);
}
