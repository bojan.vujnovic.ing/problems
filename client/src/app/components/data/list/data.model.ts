export class DataModel {
    id?: number;
    alias?: string;
    name: string;
    url: string;
}